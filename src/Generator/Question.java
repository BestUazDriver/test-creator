package Generator;
import java.util.ArrayList;
import java.util.Scanner;
public class Question {
    private String question;
    private int points;
    public ArrayList<Integer> rightNumbers;
    private ArrayList<String> answers;
    private int questionNumber;
    public Question(int questionNumber, String question, int points, ArrayList<Integer> rightNumbers, ArrayList<String> answers) {
        this.questionNumber=questionNumber;
        this.question = question;
        this.points = points;
        this.rightNumbers = rightNumbers;
        this.answers = answers;

    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {

        Scanner scanner=new Scanner(System.in);
        this.question= scanner.nextLine();
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void getRightNumbers() {
        for (int i=0 ;i<rightNumbers.size();i++){
            System.out.println((i+1)+") "+ rightNumbers.get(i));
        }
    }

    public void setRightNumbers(ArrayList<Integer> rightNumbers) {
        this.rightNumbers = rightNumbers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }

    public void getAnswers() {
        for (int i=0 ;i<answers.size();i++){
            System.out.println((i+1)+") "+ answers.get(i));
        }
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }

    @Override
    public String toString() {
        return "Question{" +
                "question='" + question + '\'' +
                ", points=" + points +
                ", rightNumbers=" + rightNumbers +
                ", answers=" + answers +
                ", questionNumber=" + questionNumber +
                '}';
    }
}
