package Generator;

import java.util.ArrayList;
import java.util.Scanner;

public class QuestionCreator {
    ArrayList<Question> qList = new ArrayList<>();
    public void CreateQuestion() {
        int n = 1;

        Scanner scanner = new Scanner(System.in);
        System.out.println("start create test");
        System.out.println("How many questions?");
        int questionsQuantity = scanner.nextInt();
        Question[] questionArr = new Question[questionsQuantity];
        System.out.println(" ");
        for (int i = 0; i < questionArr.length; i++) {
            ArrayList<Integer> rightAnswers = new ArrayList<>();
            ArrayList<String> userAnswers = new ArrayList<>();
            questionArr[i] = new Question(0, null, 0, null, null);
            System.out.println("creating question number " + (i + 1));
            System.out.println("enter the question");
            questionArr[i].setQuestion(scanner.nextLine());
            System.out.println(" ");
            while (n == 1) {
                System.out.println("enter the answers");
                userAnswers.add(scanner.next());
                System.out.println("1 more answer?");
                System.out.println("1) yes       2) no");
                n = scanner.nextInt();

            }
            System.out.println(userAnswers);
            questionArr[i].setAnswers(userAnswers);


            System.out.println(" ");
            while (n != 1) {
                System.out.println("enter the correct number(s)");
                rightAnswers.add(scanner.nextInt());
                System.out.println("1 more right answer?");
                System.out.println("1) no     2) yes");
                n = scanner.nextInt();
            }
            questionArr[i].setRightNumbers(rightAnswers);
            questionArr[i].getRightNumbers();
            System.out.println(" ");
            System.out.println("points for 1 right answer?");
            questionArr[i].setPoints(scanner.nextInt());
            System.out.println(" ");
            questionArr[i].setQuestionNumber(i + 1);
            qList.add(questionArr[i]);
            System.out.println(qList);
        }
    }
}
