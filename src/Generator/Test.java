package Generator;

import java.util.Collections;
import java.util.ArrayList;
import java.util.Scanner;

public class Test {

    public void StartTest(ArrayList<Question> qList) {
        int totalPoints=0;
        ArrayList<Question> falseAnswers=new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many questions?");
        int testQ = scanner.nextInt();
        if (testQ > qList.size()) {
            System.out.println("too many, try again");
            StartTest(qList);   //oooooooh recursiya
        } else {

            Collections.shuffle(qList);
            for (int i = 0; i < testQ; i++) {
                qList.get(i).setQuestionNumber((i+1));
                int points=0;
                ArrayList <Integer> userAnswers=new ArrayList<>();
                System.out.println("Points for 1 right question: " + qList.get(i).getPoints());
                System.out.println(qList.get(i).getQuestion());
                qList.get(i).getAnswers();
                System.out.println("enter all right numbers using space bar between numbers");
                userAnswers.add(scanner.nextInt());
                System.out.println(userAnswers);
                points= userAnswers.size()*qList.get(i).getPoints();
                for (int y=0;y< userAnswers.size();y++){
                    if (qList.get(i).rightNumbers.contains(userAnswers.get(y))==false){
                        falseAnswers.add(qList.get(i));
                        points=0;
                        break;
                    }

                }
                totalPoints=totalPoints+points;

            }
            System.out.println("Total points: "+totalPoints);
            System.out.println("wrong answers:");
            for (int i=0;i< falseAnswers.size();i++){
                System.out.print(falseAnswers.get(i).getQuestionNumber()+") ");
                System.out.println(falseAnswers.get(i).getQuestion());
                falseAnswers.get(i).getAnswers();
                System.out.println(" ");
                System.out.println("Right answer(s): ");
                falseAnswers.get(i).getRightNumbers();
                System.out.println(" ");
            }
            System.out.println("wanna start test again?");
            System.out.println("1) yes    2) no");
            int i= scanner.nextInt();
            switch(i){
                case 1:
                    StartTest(qList);
                    break;
                default: break;
            }
        }
    }
}