package Generator;
import java.util.Timer;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        int key=0;
        Test test=new Test();
        QuestionCreator create=new QuestionCreator();
        Scanner scanner=new Scanner(System.in);
        create.CreateQuestion();
        System.out.println("start test?");
        System.out.println("1) yes      2) no");
        key=scanner.nextInt();
        if (key!=1){
            System.out.println("nah, let's start anyway");
        }
        test.StartTest(create.qList);
    }
}
